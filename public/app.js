const SSO = 'http://localhost:3030'

const feathersClient = feathers().configure(feathers.rest(SSO).fetch(fetch));
feathersClient.configure(feathers.hooks());
feathersClient.configure(feathers.authentication({
    storage: window.localStorage
}));

const users = feathersClient.service('users');


$( "#add_user" ).submit(( event ) => {
  users.create({
    email: $( "input[name='email']" ).val(),
    password: $( "input[name='pass']" ).val(), 
    firstName: $( "input[name='firstName']" ).val(), 
    lastName: $( "input[name='lastName']" ).val()
  }).then(() => {
     console.log('Add user');
     showInfo('You are welcome!', 'green');
  }).catch((err) => {
     showInfo(err, 'red');
  });
  event.preventDefault();
});

$("#sign_in").submit(( event ) =>  {
  feathersClient.logout();
  feathersClient.authenticate({
    strategy: 'local',
    email: $( "input[name='email_in']" ).val(),
    password: $( "input[name='pass_in']" ).val()
  }).then((token) => {
    console.log('User is logged in', token);
    const return_to = getUrlParameter('return_to');
    if(return_to) 
      window.location.replace(decodeURIComponent(return_to)); 
  }).catch((err) => {
    showInfo(err, 'red');
  });
  event.preventDefault();
});

function showInfo(txt, color) {
  $('#info').text(txt).css('color', color);
}

//debug DRY (in Demo)
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

